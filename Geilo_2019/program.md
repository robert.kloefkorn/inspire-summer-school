Summer School 2019 in Norway
July 29 - August 10, 2020 Geilo, NORWAY

Introduction to open source software for
simulation of porous media processes.

In this first summer school of organized by the
INSPIRE project we will give and overview on
various software packages, starting with an introduction
to DUNE (https://dune-project.org),
Dumux (https://dumux.org) and OPM (https://opm-project.org)
and PorePy (https://github.com/pmgbergen/porepy).
The aim of the summer school is to give an overview on available
tools and simulations capabilities as well as an introduction to
common workflows related to the above mentioned packages.
In the second week the focus will be on
the discretization modules available and python bindings as
well as project work.

Summer school attendees should have a solid background in C++ and
numerical methods for partial differential equations.

Program

    28.7. Arrival in Geilo
    29.7. Introduction to C++
    30.7. Introduction DUNE (1/2) Finite Element Methods
    31.7. Introduction DUNE (2/2) Grid interface and linear solvers
    1.8. Introduction to Dumux
    2.8. Introduction to OPM
    3.8./4.8. Weekend (Hiking or bicycle trip)
    5.8. Introduction to DUNE-PDELab
    6.8. Introduction to DUNE-Fem(Py)
    7.8. Introduction to PorePy
    8.8. Project work
    9.8. Project work
    10.8. Departure from Geilo
