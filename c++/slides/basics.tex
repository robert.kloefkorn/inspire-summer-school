% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %

\begin{frame}[fragile]
\frametitle{Classes}

A class is a user-defined data type, that collects
\begin{itemize}
  \item data members,
  \item and member functions.
\end{itemize}

\medskip

The \emph{definition} of a class starts with the keyword \code{class}
(or \code{struct}), followed by a complete list of data members and member
functions in curly brackets.
\begin{lstlisting}
  class Foo {...};
\end{lstlisting}

\medskip

A \emph{forward declaration} consists of the \code{class} (or \code{struct})
keyword and the class name only:
\begin{lstlisting}
  class Foo;
\end{lstlisting}

\end{frame}

% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %

\begin{frame}[fragile]
\frametitle{Public and Private Class Members}

In C++, public and private members are separated:
\begin{itemize}
  \item Data members and member methods that can be called from outside
    the class are preceeded by the keyword \code{public}.
  \item Class internals \emph{(both data and methods)} that shall not be
    exported are declared as \code{private}.
\end{itemize}

\begin{lstlisting}
class Foo
{
public:
  void print();

private:
  int a;
};

Foo foo;      // create an instance of Foo
foo.print();  // call to public member method Foo::bar()
foo.a;        // error, data member method Foo::a is private!
\end{lstlisting}

\end{frame}

% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %

\begin{frame}[fragile]
\frametitle{The Difference Between \code{class} and \code{struct}}

A C++ \code{struct} is equvilant to a \code{class}.

\bigskip

The difference is that \emph{by default}, members of a class are private,
whereas members of a struct are public:

\begin{columns}
\column{.49\textwidth}
\begin{lstlisting}
  class Foo
  {
    // private data members and member methods

  public:
    // public data members and member methods
  };
\end{lstlisting}
\column{.49\textwidth}
\begin{lstlisting}
  struct Foo
  {
    // public data members and member methods

  private:
    // private data members and member methods
  };
\end{lstlisting}
\end{columns}

\end{frame}

% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %

\begin{frame}[fragile]
\frametitle{Constructors}

All data members in a class are initialized upon creation of an instance of
a class.

\medskip

Every C++ class has a special function -- the \emph{constructor} -- which
initializes the data.
\begin{lstlisting}
class Foo
{
public:
  // constructor taking two parameters
  Foo(int a, double x) : a_(a) { x_ = x; }

private:
  int a_;     // in this school, underscores
  double x_;  // are reserved for data members
};

Foo foo(1, 0.);  // create an instance of Foo
\end{lstlisting}

\medskip

The constructor method has the same name as the class, and does not have
any return type/value.

\end{frame}

% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %

\begin{frame}[fragile]
\frametitle{Copy Constructors}

The \emph{copy constructor} specifies how data members shall be initialized
from a given class instance:
\begin{lstlisting}
class Foo
{
public:
  // copy constructor
  Foo(const Foo &other) : a_(other.a_) { x_ = other.x_; }

private:
  int a_;
  double x_;
};
\end{lstlisting}

\medskip

\header{2}{Remember:} Upon calling a function like \code{bar}, a copy of
the object is created \emph{(call by value)}:
\begin{lstlisting}
  void bar(Foo foo);
  Foo foo;
  bar(foo);  // creates a copy of foo
\end{lstlisting}

\end{frame}

% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %

\begin{frame}[fragile]
\frametitle{Assignment Operator}

The \emph{assignment operator} is closely related to copying: an
\emph{existing instance} is overriden.

\medskip

\begin{lstlisting}
  class Foo
  {
  public:
    Foo &operator=(const Foo &other)
    {
      a_ = other.a_, x_ = other.x_;
      return *this;
    }

  private:
    int a_;
    double x_;
  };
\end{lstlisting}

\medskip

The first ``assignment'' below does not invoke the assignment operator:
\begin{lstlisting}
  Foo foo, foo2;
  foo = foo2;      // call to assignment operator
  Foo foo3 = foo;  // no assignment, call to copy constructor
\end{lstlisting}

\end{frame}

% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %

\begin{frame}[fragile]
\frametitle{Destructors}

The \emph{destructor} is a special member function of a class, that is called
automatically when a class instance is destroyed \emph{(e.g. when it passes out
of scope)}.

\medskip

Memory allocated by an object must be freed in the destructor.

\medskip

\begin{lstlisting}
  class Bar
  {
  public:
    Bar() : foo_(0) { foo_ = new Foo(1, 0.); }

    ~Bar()
    {
      if(foo_)
        delete foo_;
      foo_ = 0;
    }

  private:
    Foo *foo_;
  };
\end{lstlisting}

\end{frame}

% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %

\begin{frame}[fragile]
\frametitle{References}

References \emph{(like pointers)} refer to another object:
\begin{lstlisting}
  double x;
  double &ref = x;
  ref = 2;  // x = 2
\end{lstlisting}

\medskip

References cannot be uninitialized, and they cannot be initialized with
``a zero'' \emph{(unlike pointers)}.

\medskip

\begin{columns}[t]
\column{.49\textwidth}
However, a reference may be initialized from a pointer by \emph{dereferencing
the pointer}:
\begin{lstlisting}
  double *ptr = &x;
  double &ref = *ptr;
\end{lstlisting}

\medskip

Vice versa, a pointer can always be initialized from a reference:
\begin{lstlisting}
  double &ref = x;
  double *ptr = &ref;
\end{lstlisting}
\column{.49\textwidth}
\begin{figure}
  \includegraphics[width=.9\textwidth]{references}
\end{figure}
\end{columns}

\end{frame}

% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %

\begin{frame}[fragile]
\frametitle{The \code{const} Attribute}

The \emph{state} of a class instance is given by the values of all its data
members.

\medskip

A member method that does not affect the state of the object is declared
as \code{const}.

\begin{lstlisting}
  class Foo
  {
  public:
    // method does not change the value of a
    void print() const { std::cout << "a" << std::endl };

  private:
    int a;
  };
\end{lstlisting}

\end{frame}

% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %

\begin{frame}[fragile]
\frametitle{Mutable data members}

Data members may be modified in \code{const} methods if they are declared as
\code{mutable}:

\medskip

\begin{lstlisting}
class Square
{
  Square(double width) : width_(width), volume_(-1.) {}

  double volume () const
  {
    if(volume_ < 0) volume_ = width_*width_;
    return volume_;
  }

private:
  double width_;
  mutable double volume_;
};
\end{lstlisting}

\end{frame}

% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %

\begin{frame}[fragile]
\frametitle{Inheritance}

A class inherits from a base class by stating the keyword \code{public}
and the base immediately after the class name:

\vskip-1em
\begin{columns}[t]
\column{.45\textwidth}
\begin{lstlisting}
  class Base
  {
  public:
    // public members
    void foo();

  protected:
    // protected members
  };
\end{lstlisting}
\column{.55\textwidth}
\begin{lstlisting}
  class Derived : public Base
  {
    // inherits all public and
    // protected members from Base
  };

  Derived derived;
  derived.foo();
\end{lstlisting}
\end{columns}

\medskip

Protected member methods and data can only be called inside the derived class.

\end{frame}

% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %

\begin{frame}[fragile]
\frametitle{Overloading Functions}

A derived class inherits the public methods defined on the base class.
Inherited methods may be overloaded \emph{(re-defined in the derived class)}:

\vskip-1em
\begin{columns}[t]
\column{.45\textwidth}
\begin{lstlisting}
  class Base
  {
  public:
    void foo() const
    {
      std::cout << 
        "Base::foo()" << 
        std::endl;
    }

  };
\end{lstlisting}
\column{.55\textwidth}
\begin{lstlisting}
  class Derived : public Base
  {
  public:
    void foo() const
    {
      std::cout << "Derived::foo()" << std::endl;
    }
  };
\end{lstlisting}
\end{columns}

Now, the result of a call to \code{foo} differs for base and derived class
instances:
\begin{lstlisting}
  Base base;
  Derived derived;
  base.foo();     // prints "Base::foo()"
  derived.foo();  // prints "Derived::foo()"
\end{lstlisting}
\end{frame}

% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %

\begin{frame}[fragile]
\frametitle{Call to Base Class Functions}

In polymorphic classes, the base class version of an overloaded method can
still be reached:

\medskip

\begin{lstlisting}
  class Derived : public Base
  {
  public:
    void foo()
    {
      Base::foo();
      // do something
    }
  };
\end{lstlisting}

\end{frame}

% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %

\begin{frame}[fragile]
\frametitle{Virtual Functions}

We still consider two classes
\vskip-1em
\begin{columns}[t]
\column{.4\textwidth}
\begin{lstlisting}
  class Base
  {...};
\end{lstlisting}
\column{.6\textwidth}
\begin{lstlisting}
  class Derived : public Base
  {...};
\end{lstlisting}
\end{columns}

\medskip

A pointer to a derived class is compatible with a pointer to its base class:
\begin{lstlisting}
  Derived derived;
  Base *basePtr = &derived;
\end{lstlisting}

\medskip

In the above example, \code{foo} is not virtual. Calling \code{foo} from the
base class pointer results in the following output:
\begin{lstlisting}
  basePtr->foo();  // prints "Base::foo()"
\end{lstlisting}

\end{frame}

% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %

\begin{frame}[fragile]
\frametitle{Virtual Functions (Cont.)}

Overloaded functions should be declared as virtual in polymorphic classes:
the exact type of an object a pointer refers to is determined at run-time.

\vskip-1em
\begin{columns}[t]
\column{.48\textwidth}
\begin{lstlisting}
  class Base
  {
  public:
    virtual void foo() const;
  };
\end{lstlisting}
\column{.525\textwidth}
\begin{lstlisting}
  class Derived : public Base
  {
  public:
    virtual void foo() const;
  };
\end{lstlisting}
\end{columns}

\begin{lstlisting}
  Derived derived;
  Base *basePtr = &derived;
  basePtr->foo();  // prints "Derived::foo()"
\end{lstlisting}

\medskip

\header{2}{Important:} Destructors should always be virtual in polymorphic
classes!

\end{frame}

% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
%
%\begin{frame}[fragile]
%\frametitle{Cast Operators}
%
%Type casting means converting an expression of a given type into another type.
%
%
%\end{frame}
%
%% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
%
%\begin{frame}[fragile]
%\frametitle{Cast Operators}
%
%\begin{lstlisting}
%  class Base {};
%  class Derived: public Base {};
%
%  Base *basePtr = new Base;
%  Derived *derivedPtr = static_cast<Derived *>(basePtr);
%\end{lstlisting}
%
%\end{frame}
%
%% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
%
%\begin{frame}[fragile]
%\frametitle{Cast Operators}
%
%\begin{columns}[t]
%\column{.5\textwidth}
%\begin{lstlisting}
%  class Base {
%  public:
%    virtual void foo() const
%    {
%       std::cout << "Base::foo()" << std::endl;
%    }
%  };
%\end{lstlisting}
%\column{.5\textwidth}
%\begin{lstlisting}
%  class Derived : public Base {
%  public:
%    virtual void foo() const
%    {
%      std::cout << "Derived::foo()" << std::endl;
%    }
%  };
%\end{lstlisting}
%\end{columns}
%
%\begin{lstlisting}
%  Base *basePtr = new Derived;
%  Derived *derivedPtr = dynamic_cast<Derived *>(basePtr);
%\end{lstlisting}
%
%\end{frame}
%
%% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
%
%\begin{frame}[fragile]
%\frametitle{Exceptions}
%
%Run-time errors occur in various situations, e.g. during memory allocation.
%If an \emph{exception} is thrown upon an error, the user can react to it.
%
%\medskip
%
%The exception handler is declared with the \code{catch} keyword.
%
%\medskip
%
%\begin{lstlisting}
%  try {
%    // code that may throw an exception
%  }
%  catch(std::exception &e) {
%    std::cerr << e.what() << std::endl;
%  }
%\end{lstlisting}
%
%If no exception is thrown, the code continues normally and all handlers
%are ignored.
%
%\end{frame}

% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
