\beamer@endinputifotherversion {3.36pt}
\select@language {english}
\beamer@sectionintoc {1}{Compiling Programs on Linux}{5}{0}{1}
\beamer@sectionintoc {2}{Object-Oriented Programming}{7}{0}{2}
\beamer@sectionintoc {3}{Basics}{14}{0}{3}
\beamer@sectionintoc {4}{Namespaces}{29}{0}{4}
\beamer@sectionintoc {5}{Automatic Type Deduction}{33}{0}{5}
\beamer@sectionintoc {6}{Templates}{37}{0}{6}
\beamer@sectionintoc {7}{The C++ Standard Library}{50}{0}{7}
\beamer@sectionintoc {8}{Range-based for Loops}{56}{0}{8}
\beamer@sectionintoc {9}{Type Deduction for Function Argumetns}{58}{0}{9}
\beamer@sectionintoc {10}{Lambda Functions}{60}{0}{10}
\beamer@sectionintoc {11}{Dense Vector and Matrix}{64}{0}{11}
\beamer@sectionintoc {12}{References}{74}{0}{12}
