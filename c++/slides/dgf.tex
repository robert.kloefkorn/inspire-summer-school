

\section{The \dune Grid File (DGF) Format}

%\lstset{language=any}

%-----------------------------------------------------------------------------

\begin{frame}
  \frametitle{The \dune Grid File (DGF) Format}

  \header{1}{Goals:} DGF shall \dots
  \begin{itemize}
    \item \dots\ allow simple description of macro grids,
    \item \dots\ work for every \dune grid implementation (structured and unstructured),
    \item \dots\ be as independent as possible from the grid implementation,
    \item \dots\ be extendable to special grid functionalities,
    \item \dots\ easy to extend to new grids.
  \end{itemize}

  \bigskip
  \header{1}{Structure:} A DGF file consists of blocks (at least one):
  \begin{center}
  %\begin{tabular}{p{6.5em}p{6.5em}p{6.5em}p{6.5em}}
  \begin{tabular}{llll}
    \lstinline!INTERVAL! & \lstinline!SIMPLEX! & \lstinline!SIMPLEXGENERATOR! & \lstinline!PROJECTION! \\
    \lstinline!VERTEX!   & \lstinline!CUBE!    & \lstinline!GRIDPARAMETER!    & \dots
  \end{tabular}
  \end{center}

  \bigskip
  \header{2}{Warning:} The generated macro grid strongly depends on the grid implementation.
\end{frame}

%-----------------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{DGF Interval Block Example}

A DGF file consisting of a single \lstinline!INTERVAL! block will result in a
$d$-dimensional axis-aligned cube in $\R^d$.

\bigskip
An \lstinline!INTERVAL! block always consists of
\begin{itemize}
  \item two $d$-dimensional coordinates (lower left and upper right corner),
  \item the number of cells in each direction.
\end{itemize}

\bigskip
\header{1}{Example:} The Unit Square

\begin{minipage}[c]{0.48\textwidth}
\begin{lstlisting}
DGF
INTERVAL
  0  0  % first corner
  1  1  % second corner
  4  8  % 4 / 8 cells
#
\end{lstlisting}
\end{minipage}
\hfill
\begin{minipage}[c]{0.48\textwidth}%
\includegraphics[width=0.45\textwidth]{pictures/dgf/examplegrid5c}
\hfill
\includegraphics[width=0.45\textwidth]{pictures/dgf/examplegrid5s}

\centerline{\scriptsize Result depends on grid implementation}
\end{minipage}
\end{frame}

%-----------------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{DGF: Vertex and Simplex Blocks}

General simplicial grids can be constructed from a \lstinline!VERTEX! block and
a \lstinline!SIMPLEX! block.

\begin{minipage}[c]{0.72\linewidth}
\begin{lstlisting}
DGF
VERTEX
 -1.0  -1.0  % vertex 0
 -0.2  -1.0  % vertex 1
  1.0  -1.0  % vertex 2
  1.0  -0.6  % vertex 3
  1.0   1.0  % vertex 4
 -1.0   1.0  % vertex 5
  0.2  -0.2  % vertex 6
#
SIMPLEX
  0 1 5  % triangle 0, vertices 0,1,5
  1 3 6  % triangle 1, vertices 1,3,6
  1 2 3  % triangle 2, vertices 1,2,3
  6 3 4  % triangle 3, vertices 6,3,4
  6 4 5  % triangle 4, vertices 6,4,5
  6 5 1  % triangle 5, vertices 6,5,1
#
\end{lstlisting}
\end{minipage}
\hfill
\begin{minipage}[c]{0.24\linewidth}
\includegraphics[width=\linewidth]{pictures/dgf/examplegrid1s}

\centerline{\scriptsize Result (simplex grid)}
\end{minipage}
\end{frame}

%-----------------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{DGF: Vertex and Cube Blocks}

Similar to the \lstinline!SIMPLEX! block there is a \lstinline!CUBE! block.

\begin{minipage}[c]{0.72\linewidth}
\begin{lstlisting}
DGF
VERTEX
 -1.0  -1.0  % vertex 0
 -0.2  -1.0  % vertex 1
  1.0  -1.0  % vertex 2
  1.0  -0.6  % vertex 3
  1.0   1.0  % vertex 4
 -1.0   1.0  % vertex 5
  0.2  -0.2  % vertex 6
#
CUBE
  0 1 5 6  % cube 0, vertices 0,1,5,6
  1 2 6 3  % cube 1, vertices 1,2,6,3
  6 3 5 4  % cube 2, vertices 6,3,5,4
#
\end{lstlisting}
\end{minipage}
\hfill
\begin{minipage}[c]{0.24\linewidth}
\includegraphics[width=\linewidth]{pictures/dgf/examplegrid1c}

\centerline{\scriptsize Result (cube grid)}

\bigskip
\includegraphics[width=\linewidth]{pictures/dgf/examplegrid1cs}

\centerline{\scriptsize Result (simplex grid)}
\end{minipage}
\end{frame}

%-----------------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{DGF: Using Triangle or TetGen}
\begin{minipage}[c]{0.64\linewidth}
\begin{lstlisting}
DGF
SIMPLEXGENERATOR
% path to TetGen / Triangle
path $HOME/bin
% read information from file
file BBEETH1M.d_cut mesh
% dimension of grid to generate
dimension 3
#
\end{lstlisting}
\end{minipage}
\hfill
\begin{minipage}[c]{0.32\linewidth}
\includegraphics[width=\linewidth]{pictures/dgf/BBEETH1M_d_cut}

\centerline{\scriptsize Result (simplex grid)}
\end{minipage}
\end{frame}

%-----------------------------------------------------------------------------

\begin{frame}[fragile]
  \frametitle{Grid Construction Through DGF}

  The DGF parser is automatically invoked when calling one of the following
  constructors:
  \begin{lstlisting}[language=c++]
Dune::GridPtr< Grid >( const std::string &filename );
Dune::GridPtr< Grid >( std::istream &stream );
  \end{lstlisting}

  \bigskip
  The \type{Dune::GridPtr} behaves like a \type{std::shared\_ptr}, so the grid
  is (usually) deleted automatically.

  \bigskip
  \header{1}{Example:}\\
  As details in the DGF file can depend on the grid dimension (and on the world
  dimension), one can construct the grid as follows:
  \begin{lstlisting}[language=c++]
  std::stringstream name;
  name << basename << "_" << Grid::dimension << "d.dgf";
  Dune::GridPtr< Grid > gridPtr( name.str() );
  Grid& grid = *gridPtr;
  \end{lstlisting}
\end{frame}

%-----------------------------------------------------------------------------
%-----------------------------------------------------------------------------

\begin{frame}[fragile]
  \frametitle{Example for creating a unit cube with DGF}

  DGF files can be passed as input streams: 

\begin{lstlisting}[language=c++]
  int dimWorld = Grid::dimensionworld; 
  int cells = 4 ;
  // create a unit cube
  std::string dgf = "DGF\nINTERVAL\n";
  for( int i = 0; i < dimWorld; ++i )
    dgf += " 0";
  dgf += "\n";
  for( int i = 0; i < dimWorld; ++i )
    dgf += " 1";
  dgf += "\n";
  for( int i = 0; i < dimWorld; ++i )
    dgf += (" " + std::to_string( cells ));
  dgf += "\n#\n";

  std::stringstream dgfstr( dgf );
  Dune::GridPtr< Grid > gridPtr( dgfstr );
  Grid& grid = *gridPtr;
  \end{lstlisting}
\end{frame}


\begin{frame}[fragile]
  \small
  \frametitle{The Grid Selector}
  \header{1}{Observations:}
  \begin{itemize}
    \item Many applications require only a single grid.
    \item Exchanging the grid implementation by hand can be tedious.
  \end{itemize}

  \bigskip
  \header{1}{Solution:}\par
  The preprocessor can automatically define the type
  \begin{lstlisting}[language=c++]
Dune::GridSelector::GridType
  \end{lstlisting}
  based on code snipets added to \code{config.h}.

  \bigskip
  This requires \code{-DALLOW\_CXXFLAGS\_OVERWRITE=ON} and 
  \code{-DDUNE\_GRID\_GRIDTYPE\_SELECTOR=ON} set during \code{cmake} run
  to allow changing the actual type during the \code{make} procedure, e.g.,
  \begin{lstlisting}
make clean
make GRIDTYPE=ALUGRID_SIMPLEX GRIDDIM=3
  \end{lstlisting}
  Standard values for \code{GRIDTYPE}:
  \begin{center}
  \begin{tabular}{llll}
  \lstinline!ALBERTAGRID! & \lstinline!ALUGRID_CUBE! & \lstinline!ONEDGRID! & \lstinline!UGGRID! \\
  \lstinline!ALUGRID_CONFORM! & \lstinline!ALUGRID_SIMPLEX! & \lstinline!YASPGRID!
  \end{tabular}
  \end{center}
\end{frame}
