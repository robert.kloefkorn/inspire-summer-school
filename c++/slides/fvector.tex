% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %

\section{Dense Vector and Matrix}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}\frametitle{DUNE's Dense Vector and Matrix interface}

In this section, we are going to present a general-purpose template class
that can be used to represent vectors $v \in \R^n$.

The interface for vectors in DUNE is described by \\ 

  \code{DenseVector< class V >}. \\ \ \\
  %\code{FieldVector< Field, int > : public DenseVector< std::array< Field, int > >}

Two important default implementations exist: 
  \begin{itemize}
    \item \code{FieldVector< Field, int >}: Implements a vector class with a
      fixed vector length, i.e. \code{FieldVector< double, 3 >} represents $x \in \R^3$.

    \item \code{DynamicVector< Field >}: Implements a vector class with variable
      length, i.e. \code{DynamicVector< double >( 50 )} represents $x \in \R^{50}$.
      \code{DynamicVector} offers a method \code{resize} to change the length.
  \end{itemize}

  \note{All class derived from DenseVector< V > are interoperable is the size matches!}

  That a look: \alert{\url{https://dune-project.org/doxygen/master/group__DenseMatVec.html}}
\end{frame}

\begin{frame}[fragile]
\frametitle{FieldVector}

%In this section, we are going to present a general-purpose template class
%that can be used to represent vectors $v \in \R^n$.

%\medskip

The class \code{FieldVector} stores a field of static size \code{N}
of elements of type \code{T}:

\begin{lstlisting}
  template<class T, int N>
  class FieldVector 
    : public DenseVector< FieldVector< T, N > >
  {
  public:
    // STL like type definitions
    typedef T value_type;
    typedef std::size_t size_type;

    size_type size() const { return N; }
    ...

  private:
    T data_[N];
  };
\end{lstlisting}

\end{frame}

% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %

\begin{frame}[fragile]
\frametitle{Iterator Methods}

A \code{FieldVector} is a container of elements of fixed size.

\bigskip

We add iterator methods common to STL containers. That way, \code{FieldVector}
will be immediately usable with a wide range of STL algorithms.

\medskip

\begin{lstlisting}
  // STL like type definitions
  typedef value_type* iterator;
  typedef const value_type* const_iterator;

  iterator begin() { return data_; }
  const_iterator begin() const;

  iterator end() { return data_+N; }
  const_iterator end() const;
\end{lstlisting}

\end{frame}

% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %

\begin{frame}[fragile]
\frametitle{Construction and Copying}

We make use of the STL algorithms in the implementaton of the constructors,
copy constructor and assignment operator.

\medskip

\begin{lstlisting}
  explicit FieldVector() {}

  explicit FieldVector(const value_type &x)
  {
    // include <algorithm>
    std::fill(begin(), end(), x);
  }

  FieldVector(const FieldVector &other)
  {
    // include <algorithm>
    std::copy(other.begin(), other.end(), begin());
  }

  FieldVector &operator=(const FieldVector &other);
\end{lstlisting}

\end{frame}

% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %

\begin{frame}[fragile]
\frametitle{Random Access}

A \code{FieldVector} stores its elements in a sequential order
\emph{(in an array)}.

\bigskip

By overloading the operator \code{[]}, we allow random access to vector elements.

\medskip

\begin{lstlisting}
  value_type &operator[](size_type i)
  {
    return data_[i];
  }

  const value_type operator[](size_type i) const;
\end{lstlisting}

\end{frame}

% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %

\begin{frame}[fragile]
\frametitle{Vector Space Operations}

Common arithmetic operations for vectors include +, -, +=, -=, etc.

\medskip

\begin{lstlisting}
  FieldVector &operator+=(const FieldVector &other)
  {
    for(size_type i = 0; i < size(); ++i)
      (*this)[i] += other[i];
    // return reference to object
    return *this;
  }

  FieldVector operator+(const FieldVector &other) const
  {
    // implementation using operator+=
    FieldVector tmp = *this;
    return (tmp += other);
  }

  FieldVector &operator-=(const FieldVector &other);
  FieldVector operator-(const FieldVector &other) const;
\end{lstlisting}

\end{frame}

% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %

\begin{frame}[fragile]
\frametitle{Comparison Operators}

The following code examples illustrate the overloading of boolean comparison
operators.

\medskip

\begin{lstlisting}
  bool operator==(const FieldVector &other) const
  {
    // not recommended for floating point field types
    for(size_type i = 0; i < size(); ++i)
    {
      if((*this)[i] != other[i])
        return false;
    }
    return true;
  }

  bool operator!=(const FieldVector &other) const
  {
    // assert consistency with operator==
    return !(*this == other);
  }
\end{lstlisting}

\end{frame}

% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %

\begin{frame}[fragile]
\frametitle{Standard Output}

For debugging purposes, we overload the operator \lstinline!<<!.

\medskip

\begin{lstlisting}
  template<class T, int N>
  std::ostream &operator<<(std::ostream &output,
                           const FieldVector<T, N> &vector)
  {
    typedef typename FieldVector<T, N>::size_type size_type;
    for(size_type i = 0; i < vector.size(); ++i)
      output << ((i>0) ? " " : "") << vector[i];
    return output;
  }
\end{lstlisting}

\end{frame}

% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %

\begin{frame}[fragile]
\frametitle{Code Example}

This final code example shows common arithmetic operations on vectors. Due to
the overloaded operators, the syntax is very comprehensible.

\medskip

\begin{lstlisting}
  typedef FieldVector<double, 3> Vector;
  Vector x, y, z(1.);

  // x = (1., 0., 0.)
  for(Vector::size_type i = 0; i < x.size(); ++i)
    x[i] = Vector::value_type(i == 0);

  // w += 0.5 * z 
  y.axpy( 0.5, x );  

  y = x + z; // this create a temporary object 
  std::cout << y << std::endl;  // prints 2 1 1

  // determine largest element
  std::cout << *std::max_element(y.begin(), y.end()) << std::endl;
\end{lstlisting}

\end{frame}

% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %

\begin{frame}[fragile]
\frametitle{Code Example 2}

This final code example shows common operations on matrices. Due to
the overloaded operators, the syntax is very comprehensible.

\medskip

\begin{lstlisting}
  typedef FieldMatrix<double, 3 , 3>  Matrix;
  typedef FieldVector<double, 3>      Vector;
  Matrix A, B( 0 ), C(1.);

  Vector& row0 = A[ 0 ];
  // A = id
  A = 0; // assign all entries to a scalar
  for(Matrix::size_type i = 0; i < A.rows(); ++i)
    A[ i ][ i ] = 1;

  A  = B + C ; // this creates a temporary object
  A  = B; // same code but better implementation 
  A += C;

  Vector x(1), b(0);
  // A * x = b 
  A.mv( x, b ); // also mtv, umv, umtv 
  A.invert(); // compute A^-1 (N < 1000)

  // print matrix 
  std::cout << A << std::endl;
\end{lstlisting}

\end{frame}

% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
