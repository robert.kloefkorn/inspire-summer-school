#include <cstddef>

#include <algorithm>

#include <dune/common/fvector.hh>
#include <dune/geometry/quadraturerules.hh>

#include <dune/python/geometry/quadraturerules.hh>
#include <dune/python/grid/entity.hh>
#include <dune/python/pybind11/numpy.h>
#include <dune/python/pybind11/pybind11.h>

struct QuadraticShapeFunctions
{
  static const int dimension = 2;
  static constexpr std::size_t locNrDofs () { return 6; }

  typedef Dune::FieldVector< double, dimension > LocalCoordinate;

  void evaluate ( const LocalCoordinate &x, std::vector< double > &v ) const
  {
    const double bary[ 3 ] = { 1.0 - x[ 0 ] - x[ 1 ], x[ 0 ], x[ 1 ] };
    for( int i = 0; i < 3; ++i )
    {
      v[ i ] = bary[ i ] * (2.0*bary[ i ] - 1.0);
      v[ i+3 ] = 4.0 * bary[ (3-i)%3 ] * bary[ (4-i)%3 ];
    }
  }

  void gradient ( const LocalCoordinate &x, std::vector< LocalCoordinate > &v ) const
  {
    const double bary[ 3 ] = { 1.0 - x[ 0 ] - x[ 1 ], x[ 0 ], x[ 1 ] };
    const LocalCoordinate dbary[ 3 ] = { { -1.0, -1.0 }, { 1.0, 0.0 }, { 0.0, 1.0 } };
    std::fill( v.begin(), v.end(), LocalCoordinate{ 0.0, 0.0 } );
    for( int i = 0; i < 3; ++i )
    {
      v[ i ].axpy( 4.0 * bary[ i ] - 1.0, dbary[ i ] );
      v[ i+3 ].axpy( 4.0 * bary[ (4-i)%3 ], dbary[ (3-i)%3 ] );
      v[ i+3 ].axpy( 4.0 * bary[ (3-i)%3 ], dbary[ (4-i)%3 ] );
    }
  }

  template< class Mapper, class Entity >
  void map ( const Mapper &mapper, const Entity &element, std::vector< std::size_t > &indices )
  {
    static_assert( Mapper::GridView::dimension == dimension, "Grid must be 2d" );
    for( int i = 0; i < 3; ++i )
    {
      indices[ i ]   = mapper.subIndex( element, i, 2 );
      indices[ i+3 ] = mapper.subIndex( element, i, 1 );
    }
  }
};

template< class Mapper, class RHS, class Mass, class V, class VI, class VD >
void assemble ( const Mapper &mapper, int order,
                const RHS &f, const Mass &m, double D,
                V &rhsVec, VI &rowIndexVec, VI &colIndexVec, VD &valueVec )
{
  typedef typename Mapper::GridView::template Codim< 0 >::Geometry Geometry;

  auto localF = localFunction( f );
  auto localM = localFunction( m );

  auto rhs = rhsVec.template mutable_unchecked< 1 >();
  auto rowIndex = rowIndexVec.template mutable_unchecked< 1 >();
  auto colIndex = colIndexVec.template mutable_unchecked< 1 >();
  auto values = valueVec.template mutable_unchecked< 1 >();

  QuadraticShapeFunctions shapeFunctions;
  typedef Dune::QuadratureRules< double, QuadraticShapeFunctions::dimension > QuadratureRules;

  const std::size_t locNrDofs = shapeFunctions.locNrDofs();
  std::vector< double > phi( locNrDofs );
  std::vector< typename Geometry::LocalCoordinate > dphi( locNrDofs );
  std::vector< typename Geometry::GlobalCoordinate > dphiGlobal( locNrDofs );
  std::vector< std::size_t > indices( locNrDofs );

  std::vector< double > localMatrix( locNrDofs * locNrDofs );

  std::size_t count = 0;
  for( const auto &entity : elements( mapper.gridView() ) )
  {
    const Geometry geo = entity.geometry();
    auto rule = QuadratureRules::rule( geo.type(), order );

    std::fill( localMatrix.begin(), localMatrix.end(), 0 );

    // make vectorized call to Python GridFunction possible
    // check shape of valuesArray here...
    auto pointsWeights = Dune::Python::quadratureToNumpy( rule );

    localM.bind( entity );
    auto massArray = localM( pointsWeights.first ).template cast< pybind11::array_t< double > >();
    auto massValues = massArray.template unchecked< 1 >();
    localM.unbind();
    localF.bind( entity );
    auto rhsArray  = localF( pointsWeights.first ).template cast< pybind11::array_t< double > >();
    auto rhsValues  = rhsArray.template unchecked< 1 >();
    localF.unbind();

    shapeFunctions.map( mapper, entity, indices );

    for( std::size_t p = 0; p < rule.size(); ++p )
    {
      const auto &qp = rule[p];
      const auto &hatx = qp.position();
      double weight = qp.weight() * geo.integrationElement( hatx );

      shapeFunctions.evaluate( hatx, phi );
      for( std::size_t i = 0; i < locNrDofs; ++i )
        rhs[ indices[ i ] ] += (weight * rhsValues[ p ]) * phi[ i ];

      shapeFunctions.gradient( hatx, dphi );
      auto jit = geo.jacobianInverseTransposed( hatx );
      for( std::size_t i = 0; i < locNrDofs; ++i )
        jit.mv( dphi[ i ], dphiGlobal[ i ] );

      for( std::size_t i = 0; i < locNrDofs; ++i )
        for( std::size_t j = 0; j < locNrDofs; ++j )
          localMatrix[ i*locNrDofs + j ] +=
            ( D * (dphiGlobal[ j ] * dphiGlobal[ i ]) +
              massValues[ p ] * phi[ j ] * phi[ i ] ) * weight;
    }

    shapeFunctions.map( mapper, entity, indices );
    for( std::size_t i = 0; i < locNrDofs; ++i )
    {
      for( std::size_t j = 0; j < locNrDofs; ++j, ++count )
      {
        rowIndex[ count ] = indices[ i ];
        colIndex[ count ] = indices[ j ];
        values[ count ]   = localMatrix[ i*locNrDofs + j ];
      }
    }
  }
}
