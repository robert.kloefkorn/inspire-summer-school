# <markdowncell>
# # Grid Construction
# The simplest approach to construct a grid is to
# use the `structuredGrid` functions which takes
# coordinates of the lower left and upper right
# corner of the domain and the subdivision to use
# in each coordinate direction:

# <codecell>
from dune.grid import structuredGrid
view = structuredGrid([-0.5,-0.5],[2,1],[10,20])
view.plot()

# <markdowncell>
# To construct more complicated grids, we
# can use Python dictionaries which stores key/value
# pairs in the form
# ```
# `{key1:value1, key2:value2,...}`.
# ```
# For example to construct a triangular grid
# using 'ALUGrid`:
# <codecell>
from dune.alugrid import aluConformGrid
mesh = { "vertices":[ [ 0, 0], [ 1, 0], [ 1, 1],
                      [-1, 1], [-1,-1], [ 0,-1] ],
         "simplices": [ [2,0,1], [2,3,0], [3,4,0], [0,4,5] ] }
view = aluConformGrid(constructor=mesh)
view.plot()

# <markdowncell>
# ## Using the grid:
# The most common task is to iterate over the elements
# (codimension zero entities) of the grid.
# This can be done with a simple for loop
# ```
# for element in view.elements:
# ```
# The element provides all its geometric
# information through the `geometry` property.
# The returned object provides all information
# concerning the mapping from reference into
# physical space, i.e., the center of the element
# its volume, the reference mapping, or the coordinats
# of the corners using `element.geometry.corners` which returns a vector
# of the corner points (in fact it returns a list of `Dune::FiledVector`
# instances.
# <codecell>
for element in view.elements:
    geo = element.geometry
    print(geo.volume,geo.center,geo.toGlobal([1./3,1./3]))

# <markdowncell>
# ## Grid Functions
# It's also quite easy to construct functions
# defined over the grid, i.e., grid-functions.
# These are functions that can be evaluated given
# an element and a local coordinate.
# Grid functions are usual Python functions either
# of the form `def f(globalx)` or `def f(localx)`
# using a Python decorator:
# <codecell>
from dune.grid import gridFunction
from math import sin, log, pi
@gridFunction(view)
def f1(y):
    return sin(20*y[0]*y[1])*y.two_norm
f1.plot()
@gridFunction(view)
def f2(element,localx):
    y = element.geometry.toGlobal(localx)
    return sin(20*y[0]*y[1])*y.two_norm
f2.plot()
@gridFunction(view)
def center(element,localx):
    return element.geometry.center.two_norm
center.plot()
# <markdowncell>
# Independent of the signature of the function used,
# one can evaluate each grid function given an element
# and a local coordinate:
# <codecell>
for element in view.elements:
    xLocal = (1./3.,1./3.) # can use the referenceElement property
    print(f1(element,xLocal),f2(element,xLocal))
# <markdowncell>
# Alternative using `bind` (closer to C++ version):
# <codecell>
localF = center.localFunction()
for element in view.elements:
    localF.bind(element)
    xLocal = (1./3.,1./3.) # can use the referenceElement property
    print(localF(xLocal))
    localF.unbind()

# <markdowncell>
# Plotting is done using matplotlib (pyplot)
# <codecell>
from matplotlib import pyplot
figure = pyplot.figure(figsize=(30,10))
pyplot.subplot(1,3,1)
f1.plot(figure=figure,colorbar=False)
pyplot.subplot(1,3,2)
f1.plot(level=5,gridLines="white",colorbar=False,
        figure=figure)
view.hierarchicalGrid.globalRefine(3)
pyplot.subplot(1,3,3)
f1.plot(gridLines="white",colorbar=False,figure=figure)
pyplot.show()

# <markdowncell>
# Finally we can write vtk files which can be viewed
# with some third party software e.g. `paraview` -
# those programs want the `named` data so we use a
# dictionary for that:
# <codecell>
view.writeVTK("sin", pointdata={"sin":f1})

# <markdowncell>
# ## Attaching data to the grid
# For this we use a `mapper` $g_T$ provided by the
# grid. It is constructed by passing in a `layout`,
# i.e., how many degrees of freedom are needed per
# subentity (in fact per `geometry` type...)
#
# Let's attach two degree of freedom to each
# vertex and one degree of freedom to each edge:
# <codecell>
from dune.grid import cartesianDomain
domain = cartesianDomain([0, 0], [1, 1], [3, 2])
view = aluConformGrid(domain)
def layout(geometryType):
    if geometryType.dim == 0:
        return 2 # this is a vertex
    elif geometryType.dim == 1:
        return 1 # this is an edge
    else:
        return 0
mapper = view.mapper(layout)
print("number of degrees of freedom:",len(mapper))
# <markdowncell>
# The easiest way to access all indices attach to an element, i.e.,
# the vector $\big(g_T(i)\big)_i$, # is to call the method
# `indices=mapper(element)`.
# The vector of indices contains first the indices for the vertices
# then come the edges,..., down (or up) to the element's global indices.
# The order for each set of subentities depends on their
# numbering in the Dune reference element.
# <codecell>
for element in view.elements:
    print("all indices on element:", mapper(element))

# <markdowncell>
# Now to some 'real' example:
# we compute the linear interpolation of a grid function $u$
# over a triangular grid.
#
# We need to attach one degree of freedom to each vertex $p$
# which corresponds to the value of the function at $p$.
# On each triangle with vertices $p_0,p_1,p_2$
# the discrete function is now given by the unique
# linear function taking the values
# $u(p_0),u(p_1),u(p_2)$
# at the three vertices. This function is
# \begin{align*}
#   u_h(e,\hat x) = u(p_0)(1-\hat x_0-\hat x_1)+u(p_1)\hat x_0+u(p_2)\hat x_1
# \end{align*}
# We store the degrees of freedom in a numpy array:
# <codecell>
import numpy
view = aluConformGrid(constructor=mesh)
view.hierarchicalGrid.globalRefine(3)
@gridFunction(view)
def u(y):
    return sin(2*y[0]*y[1])*y.two_norm
def linearInterpolation():
    layout = lambda gt: 1 if gt.dim == 0 else 0
    mapper = view.mapper(layout)
    data = numpy.zeros(len(mapper))
    for e in view.elements:
        indices = mapper(e)
        for corner in range(len(indices)):
            data[indices[corner]] = u(e.geometry.corners[corner])
    return mapper,data

mapper, data = linearInterpolation()

@gridFunction(view)
def uh(e, x):
    bary = 1-x[0]-x[1], x[0], x[1]
    idx = mapper(e)
    return sum(b * data[i] for b, i in zip(bary, idx))
uh.plot(figsize=(4,4), gridLines=None)

# <markdowncell>
# Let's compute the interpolation error by
# comparing the linear interpolation at the
# center of each element with the value of 'f'
# there - we can also compute the
# 'experimental order of convergence' (EOC)
# on a sequence of globally refined grids. This is a simple
# proceedure to test if a numerical scheme works. Assume
# for example that one can prove that the error $e_h$ on a
# grid with grid spacing $h$ satisfies
# \begin{align*}
#   e_h \approx Ch^p
# \end{align*}
# then
# \begin{align*}
#   \log \frac{e_h}{e_H} \approx p\log \frac{h}{H}
# \end{align*}
# which can be used to get a good idea about the convergence rate $p$
# using the errors computed on two different grids.
# <codecell>
@gridFunction(view)
def error(e, x):
    return abs(uh(e,x)-u(e,x))
hatx = [1./3., 1./3.]
err = max(error(e, hatx) for e in view.elements)
print(view.size(0), err)
for i in range(5):
    oldErr = err
    view.hierarchicalGrid.globalRefine(2)
    mapper, data = linearInterpolation()
    err = max(error(e, hatx) for e in view.elements)
    eoc = log(oldErr/err)/log(2)
    print(view.size(0), err, eoc)

uh.plot(figsize=(4,4), gridLines=None)

# <markdowncell>
# ## Quadrature
# Finally we turn our attention to computing
# integrals of grid functions by iterating over
# the elements and computing the integral on
# the element using a quadrature rule:
# <codecell>
from dune.grid import cartesianDomain
from dune.geometry import quadratureRule

domain = cartesianDomain([0, 0], [1, 1], [50, 50])
view = aluConformGrid(domain)
view.hierarchicalGrid.globalRefine(5)
@gridFunction(view)
def f(x): return sin(3*pi*x[0])*sin(3*pi*x[1])

value = 0
for e in view.elements:
    geo = e.geometry
    for p in quadratureRule(e.type, 4):
        x = p.position
        w = p.weight * geo.integrationElement(x)
        y = geo.toGlobal(x)
        value += w * f(y)
print("integral:", value)

# <markdowncell>
# To be closer to the C++ version here the
# same thing using bind/unbind
# <codecell>
value = 0
lf = f.localFunction()
for e in view.elements:
    lf.bind(e)
    geo = e.geometry
    for p in quadratureRule(e.type, 4):
        x = p.position
        w = p.weight * geo.integrationElement(x)
        value += w * lf(x)
    lf.unbind()
print("integral:", value)

# <markdowncell>
# Now that we have a version which is close to the C++
# code, we can also directly implement this based on the C++
# interface and call that function from within Python -
# this speeds up things quite a bit as soon which becomes
# important once the grids are bigger or the algorithms
# are more complex:
# <codecell>
from dune.generator import algorithm
value = algorithm.run('integrate', 'integrate.hh',\
                      view, f, 4)
print("integral:", value)

# <markdowncell>
# Another way to speed things up is to use
# 'vectorization' available with numpy vectors.
# That means that a function is called for a number
# of arguments at the same time and returns a vector containing
# the evaluation of the function at these arguments. We need to
# use the numpy functions like `numpy.sin` in the definition
# of our function. The coordinates have to be passed in as
# a numpy vector of the form:
# ```
# numpy.array([ [x0,x1,x2,...],[y0,y1,y2,...] ])
# ```
# so not! in the form `[ [x0,y0],[x1,y1],...]` which might
# require transposing a given vector of coordinates.
# The result is then the vector
# ```
# numpy.array([ f(x0,y0),f(x1,y1),f(x2,y2),... ])
# ```
# <codecell>
view = aluConformGrid(constructor=mesh)
view.hierarchicalGrid.globalRefine(1)
@gridFunction(view)
def f(x): return numpy.sin(3*pi*x[0])*numpy.sin(3*pi*x[1])
print("At the corners of the domain:")
print( f( numpy.array( [[0,1,0,1],[0,0,1,1]]) ))
print("At the corners of each element:")
for e in view.elements:
    corners = numpy.array(e.geometry.corners).transpose()
    print(f(corners))
# <markdowncell>
# Since quadratures are so important there are some
# utility functions to compute them using vectorization:
# <codecell>
from dune.geometry import quadratureRules, integrate
view.hierarchicalGrid.globalRefine(3)
value = 0
rules = quadratureRules(4)
for e in view.elements:
    value += integrate(rules,e,f)
print("integral:",value)
# <markdowncell>
# And we can also vectorize the callback from C++ into Python
# which is shown in the `integrateVectorized` function in
# `integrate.hh`.
#
# Using vectorization we can also shorten and
# speed up the function computing the linear interpolation:
# <codecell>
def linearInterpolation():
    layout = lambda gt: 1 if gt.dim == 0 else 0
    mapper = view.mapper(layout)
    data = numpy.zeros(len(mapper))
    for e in view.elements:
        corners = numpy.array(e.geometry.corners).transpose()
        data[mapper(e)] = f(corners)
    return mapper,data
mapper, data = linearInterpolation()
@gridFunction(view)
def uh(e, x):
    bary = 1-x[0]-x[1], x[0], x[1]
    idx = mapper(e)
    return sum(b * data[i] for b, i in zip(bary, idx))
uh.plot(figsize=(9,9), gridLines="white")
