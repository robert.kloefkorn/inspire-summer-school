import numpy, math
import scipy.sparse
from scipy.sparse.linalg import spsolve
import dune.geometry
from dune.grid import cartesianDomain, gridFunction
from dune.alugrid import aluConformGrid
from dune.generator import algorithm

class LagrangeSpace:
    def __init__(self,view,localDofs,layout):
        self.view   = view
        self.dim    = view.dimension
        self.mapper = view.mapper(layout)
        self.localDofs = localDofs
    def __len__(self):
        return len(space.mapper)
    def function(self,dofVector):
        @gridFunction(view)
        def uh(e,x):
            indices = self.mapper(e)
            phiVals = self.evaluateLocal(x)
            localDofs = dofVector[indices]
            return numpy.dot(localDofs, phiVals)
        return uh
class QuadraticLagrangeSpace(LagrangeSpace):
    def __init__(self,view):
        LagrangeSpace.__init__(self,view,6,
                    lambda gt: 1 if gt.dim == 0 or gt.dim == 1 else 0)
    def evaluateLocal(self, x):
        bary = 1.-x[0]-x[1], x[0], x[1]
        return numpy.array([ bary[i]*(2.*bary[i]-1.) for i in range(3) ] +\
               [ 4.*bary[(3-j)%3]*bary[(4-j)%3] for j in range(3) ])

def assemble(space,force,mass,D):
    # storage for right hand side
    rhs = numpy.zeros(len(space))

    # data structure for global matrix using COO format
    localEntries = space.localDofs
    globalEntries = localEntries**2 * space.view.size(0)
    value = numpy.zeros(globalEntries)
    rowIndex, colIndex = numpy.zeros(globalEntries,int), numpy.zeros(globalEntries,int)

    algorithm.run("assemble", "assembly.hh",
            space.mapper, 4, force, mass, D, rhs, rowIndex, colIndex, value)

    matrix = scipy.sparse.coo_matrix((value, (rowIndex, colIndex)),
                         shape=(len(space),len(space))).tocsr()

    return rhs,matrix

def error(u,uh):
    @gridFunction(view)
    def error(e,x):
        return (uh(e,x)-u(e,x))**2
    rules = dune.geometry.quadratureRules(4)
    ret = 0
    for e in view.elements:
        ret += dune.geometry.integrate(rules,e,error)
    ret = math.sqrt(ret)
    return ret

domain = cartesianDomain([0, 0], [1, 1], [10, 10])
view   = aluConformGrid(domain)

@gridFunction(view)
def mass(p):
    return numpy.array([1.])
@gridFunction(view)
def u(p):
    x,y = p
    return numpy.cos(2*numpy.pi*x)*numpy.cos(2*numpy.pi*y)
@gridFunction(view)
def forcing(p):
    return u(p)*((2*numpy.pi)**2*2+mass(p))
# u.plot(level=3)

for step in range(5):
    space = QuadraticLagrangeSpace(view)
    rhs,matrix = assemble(space, forcing, mass, 1.)
    # dofs = spsolve(matrix,rhs)
    # uh = space.function(dofs)
    print(view.size(0))# , error(u,uh))
    # uh.plot(level=1)
    view.hierarchicalGrid.globalRefine(2)
