#!/bin/bash

mkdir $HOME/codegen
cd $HOME/codegen

PYENVDIR=$HOME/codegen

# create necessary python virtual environment
if ! test -d pdelab-env ; then
  # do not use python3 here or Dune will not pick up virtualenv correctly
  python3.6 -m venv $PYENVDIR/pdelab-env
  source $PYENVDIR/pdelab-env/bin/activate
  pip install --upgrade pip
fi
source $PYENVDIR/pdelab-env/bin/activate

git clone https://gitlab.dune-project.org//robert.kloefkorn/inspire-school-2019.git

cd inspire-school-2019
# ./install.sh

# leave venv
deactivate
