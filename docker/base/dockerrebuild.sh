# warning: this removes all docker related images,container,volumes so be # careful...
docker system prune -a -f --volumes
docker rmi $(docker images -q)
#docker volume rm dunepy
docker volume rm school
docker system df

#docker build . --no-cache -t registry.dune-project.org/dune-fem/dune-fem-dev:latest
docker build . --no-cache -t registry.dune-project.org/robert.kloefkorn/inspire-summer-school/school:latest
