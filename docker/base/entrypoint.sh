#!/usr/bin/env bash

if [ ! $groupId -eq 0 ] && [ ! $userId -eq 0 ] ; then
  # echo "switching user:group id from `id -u dune`:`id -g dune` to $userId:$groupId"
  chgrp -R -h $groupId /school
  chown -R -h $userId /school
  usermod -u $userId dune
  groupmod -o -g $groupId dune
  echo "###############################################################"
  echo "# Welcome to the Geilo school docker development environment. #"
  echo "# The Dune git repositories are located at '/school/DUNE'    .#"
  echo "# A python virtual environment is active and Python           #"
  echo "# scripts can be found in '/school/DUNE/dune-fempy/docs'.     #"
  echo "###############################################################"
  exec gosu dune bash
else
  # echo "not setting any user/group id - will start as root"
  exec bash
fi
