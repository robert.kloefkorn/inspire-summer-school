#!/usr/bin/env bash

# dockerName=registry.dune-project.org/robert.kloefkorn/inspire-summer-school/school:latest
dockerName=registry.dune-project.org/smuething/inspire-summer-school/school2
userName=school

# Is Docker installed?
haveDocker=$(docker -v)
if [ ! $? -eq 0 ];
then
  echo "Docker could not be found on your system."
  echo "Please install docker following instructions for you operating system from"
  echo "https://docs.docker.com."
  exit 1
fi


# check operating system and start docker
if [ $(uname) = "Linux" ] ;
then
  # need to check if docker is called using 'sudo'
  if [ ! "$SUDO_UID" = "" ] ;
  then
    USERID=$SUDO_UID
  else
    USERID=$(id -u)
  fi
  if [ ! "$SUDO_GID" = "" ] ;
  then
    GROUPID=$SUDO_GID
  else
    GROUPID=$(id -g)
  fi

  # now start docker container
  xhost +si:localuser:$USER
  if [ ! "$(docker container ls -a | grep $userName)" ] ;
  then
    docker run -it --name $userName -v $PWD:/host -v $userName:/$userName \
      -v /tmp/.X11-unix:/tmp/.X11-unix:ro --device /dev/dri \
      -p 127.0.0.1:8888:8888 \
      -e userId=$USERID -e groupId=$GROUPID --hostname="$userName" --add-host $userName:127.0.0.1 $dockerName
  else
    docker start -i $userName
  fi
  xhost -si:localuser:$USER
elif [ $(uname) = "Darwin" ] ;
then
  echo "on MAC: for X forwarding remember to run"
  echo '    socat TCP-LISTEN:6000,reuseaddr,fork UNIX-CLIENT:\"$DISPLAY\"'
  echo "in separate terminal"
  echo ""
  echo "also note that to run this image docker must be configured to have access to at least 4GB of memory"
  echo "this can be set in the 'advanced' tab of the docker toolbar UI; another option is to run"
  echo "docker-machine stop"
  echo "VBoxManage modifyvm default --memory 4096"
  echo "docker-machine start"
  echo "Changing the maximal number of cores can also increase performance:"
  echo "VBoxManage modifyvm default --cpus 4"
  echo ""
  xhost +si:localuser:$USER
  if [ ! "$(docker container ls -a | grep $userName)" ] ;
  then
    docker run -it -v $PWD:/host -v $userName:/$userName --name $userName \
      -v /tmp/.X11-unix:/tmp/.X11-unix:ro \
      -e DISPLAY=$(ipconfig getifaddr en0):0 --net=host \
      -e userId=$(id -u) -e groupId=$(id -g) --hostname="$userName" --add-host $userName:127.0.0.1 $dockerName
  else
    docker start -i $userName
  fi
  xhost -si:localuser:$USER
elif [ $(uname) = "MINGW64_NT-10.0" ] ;
then
  # https://dev.to/darksmile92/run-gui-app-in-linux-docker-container-on-windows-host-4kde
  echo "On Windows: for X forwarding you will need an x-seerver app running use for example vcxsrv -"
  echo "start it with \"xlaunch\" and tick \"disable access control\""
  echo ""
  echo "also note that to run this image docker must be configured to have access to at least 4GB of memory"
  echo "this can be set in the 'advanced' tab of the docker menu."
  echo "Changing the maximal number of cores can also increase performance:"
  echo ""
  if [ ! "$(docker container ls -a | grep $userName)" ] ;
  then
    if [ -z "$DISPLAY" ] || [ "$DISPLAY" == "needs-to-be-defined" ];
    then
      export DISPLAY=$(ipconfig | grep "IPv4" | head -1 | grep -oE '[^ ]+$'):0
      echo "Setting DISPLAY to $DISPLAY"
    fi
    docker run -it --rm -v "$PWD":/host -v $userName:/$userName --name="$userName" \
      -e DISPLAY=$DISPLAY --privileged \
      -e userId=$(id -u) -e groupId=$(id -g) $dockerName
  else
    docker start -i $userName
  fi
else
  echo "System not tested on your architecture identified as $uname"
fi
