#!/usr/bin/env bash

dockerName=registry.dune-project.org/robert.kloefkorn/inspire-summer-school/school:latest

if [ $(uname) = "Linux" ]
then
  if [ ! "$SUDO_UID" = "" ] ;
  then
    USERID=$SUDO_UID
  else
    USERID=$(id -u)
  fi
  if [ ! "$SUDO_GID" = "" ] ;
  then
    GROUPID=$SUDO_GID
  else
    GROUPID=$(id -g)
  fi
  xhost +si:localuser:$USER
  docker run -it --rm -v $PWD:/host -v school:/school \
    -v /tmp/.X11-unix:/tmp/.X11-unix:ro --device /dev/dri \
    -e userId=$USERID -e groupId=$GROUPID --hostname="school" --add-host school:127.0.0.1 $dockerName
  xhost -si:localuser:$USER
elif [ $(uname) = "Darwin" ]
then
  echo "on MAC: for X forwarding remember to run 'ipconfig getifaddr en0)' in separate terminal"
  xhost +si:localuser:$USER
  docker run -it --rm -v $PWD:/host -v school:/school \
    -v /tmp/.X11-unix:/tmp/.X11-unix:ro \
    -e DISPLAY=$(ipconfig getifaddr en0):0 --net=host \
    -e userId=$(id -u) -e groupId=$(id -g) --hostname="school" --add-host school:127.0.0.1 $dockerName
  xhost -si:localuser:$USER
else
  echo "uname=$(uname) not recognized - specific settings for 'Linux' and 'Darwin' available"
  echo "starting docker container without X forwarding."
  docker run -it --rm -v $PWD:/host -v school:/school \
    -e userId=$(id -u) -e groupId=$(id -g) --hostname="school" --add-host school:127.0.0.1 $dockerName
fi
