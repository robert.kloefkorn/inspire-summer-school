1) Changes to runschooldocker.sh (around line 40):
   to the docker run command add `-p 127.0.0.1:8888:8888`
2) within the docker container run 'load_school'
3) copy 'jupyter_config.py' to '/host'
4) run 'jupyter-notebook --config=/etc/jupyter_config.py'
5) in a web server open 'http://127.0.0.1:8888' - password is 'dune'
