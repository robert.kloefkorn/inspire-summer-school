#!/bin/bash

OLDDIR=`pwd`

source /school/dune-env/bin/activate
#export DUNE_CONTROL_PATH=$HOME/DUNE:.

# delete DUNE python cache
rm -rf $HOME/dune-env/.cache/dune-py

DUNEDIR=/school/DUNE
cd $DUNEDIR

# delete unused modules
rm -rf dune-pdelab
rm -rf dumux

cd dune-fem
git fetch origin
git checkout master
git reset --hard origin/master
cd $DUNEDIR

cd dune-fempy
git pull

cd $DUNEDIR

rm -rf dune-python
git clone -b bugfix/modules https://gitlab.dune-project.org/staging/dune-python.git

./dune-common/bin/dunecontrol --opts=config.opts --module=dune-vem all

# install all python modules in the pip environment
./dune-python/bin/setup-dunepy.py --opts=config.opts install

# compile some grids for python
cd dune-fempy/doc
python dune-fempy.py
cd ../school
python parabolic.py
python heatrobin.py
python reactionsystem.py
python mcfSphere.py
python mcfSoap.py
python spiral.py
python spiralAdapt.py
##################################################################

cd $OLDDIR
