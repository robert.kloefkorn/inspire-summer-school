# <markdowncell>
# # A linear transport problem using a finite volume scheme
# We want to solve
# \begin{align*}
#   \partial_t c(x,t) + u\cdot\nabla c(x,t) &= 0 \\
#   c(x,0) &= c_0(x)
# \end{align*}
# where $u$ is a constant velocity and $c_0$ is a given function (we also
# have some boundary conditions which I'm ignoring here).
#
# We want to use an explicit finite volume scheme
# \begin{align*}
#   c^{n+1}_E &= c^n_E - \frac{\tau^n}{|E|}\sum_{N} g_{EN}(c^n_E,c^n_N)
# \end{align*}
# where $c^n_E$ is a single degree of freedom on each element $E$
# approximating the average of $c(\cdot,t^n)$ over the element $E$.
# The time step from $t^n$ to $t^{n+1}$ is given by $\tau$.
# <codecell>
import numpy, math
from dune.grid import structuredGrid, gridFunction, GridFunction
from dune.plotting import block
block = False

# <markdowncell>
# Compute the evolution from $t^n$ to $t^{n+1}$, so given a vector of
# unknowns $(c_E)_E$ we compute an update vector
# \begin{align*}
#   v_E = \frac{\tau^n}{|E|}\sum_{N} g_{EN}(c_E,c_N)
# \end{align*}
# and then set $c -= v$.
# <codecell>
def evolve(view, mapper, c, bnd, velocity):
    upd, dt = numpy.zeros(len(c)), 1e100
    for e in view.elements:
        volume, idxi = e.geometry.volume, mapper.index(e)
        sumfactor = 0.0
        for i in view.intersections(e):
            f = (velocity * i.centerUnitOuterNormal) * i.geometry.volume / volume
            sumfactor += max(f, 0)
            outside = i.outside
            if outside is not None:
                upd[idxi] -= c[idxi if f > 0 else mapper.index(outside)] * f
            elif i.boundary:
                upd[idxi] -= (c[idxi] if f > 0 else bnd(i.geometry.center)) * f
        dt = min(dt, 1.0 / sumfactor)
    dt *= 0.99
    c += upd*dt
    return dt

# <markdowncell>
# Now we come to the main program in which we set up the grid
# <codecell>
view = structuredGrid([0,0],[1,1],[20,20])
# <markdowncell>
# define the problem data
# <codecell>
@gridFunction(view)
def c0(x):
    return 1.0 if x.two_norm > 0.125 and x.two_norm < 0.5 else 0.0
@GridFunction(view)
class Bnd:
    def __init__(self,t):
        self.t = t
    def __call__(self,x):
        return c0(x - [self.t, self.t])
velocity = [1,]*view.dimensionworld

# <markdowncell>
# setup the discrete data
# <codecell>
mapper = view.mapper(lambda gt: gt.dim == view.dimension)
c = numpy.zeros(len(mapper))
for e in view.elements:
    c[mapper.index(e)] = c0(e.geometry.center)

@gridFunction(view)
def uh(e,x):
    indices   = mapper(e)
    return c[indices[0]]

# <markdowncell>
# and do the time evolution
# <codecell>
t = 0.0
plotStep = 0.05
nextPlot = 0.05
while t < 0.5:
    t += evolve(view, mapper, c, Bnd(t), velocity)
    print(t)
    if t > nextPlot:
        uh.plot()
        nextPlot += plotStep
uh.plot()

# <markdowncell>
# # Some tasks:
#
# This can be extended in many ways for example:
#
# 1. do the evolution in C++ and use an `algorithm`
#
# 2. add a diffusion term following the lecture from yesterday
#
# Solution: if you've done this please share you're solution with us!
#
# <codecell>

