# <markdowncell>
# # A projection into a finite element space
# The following requires assembly a finite element
# matrix (the mass matrix) and a right hand side.
# We use linear Lagrange shape functions.
#
# So we are looking for the $L^2$ projection
# \begin{align*}
# u_h(x) = \sum_k u_k\varphi_k(x)
# \end{align*}
# which is the solution of
# \begin{align*}
#   \int_\Omega u_h\varphi_i &= \int_\Omega u\varphi_i, && \text{for all $i$}
# \end{align*}
# We assume that on an element $E$ we have
# \begin{align*}
#   \varphi_{\g_E(k)}\,(x) = \hat\varphi_k\,(F_E^{-1}(x))
# \end{align*}
# for $k=0,1,2$ and where $g_E$ denotes the local to global dof mapper
# and $F_E$ is the reference mapping.
#
# So we need to compute
# \begin{align*}
#   M^E_{kl} := \int_\hat{E} |DF|\hat\varphi_k\hat\varphi_l~, &&
#   b^E_l := \int_E u\varphi_l~,
# \end{align*}
# and distribute these into a global matrix.
# <codecell>
import numpy, math
import scipy.sparse
import scipy.sparse.linalg
from dune.geometry import quadratureRules, quadratureRule, integrate
from dune.grid import cartesianDomain, gridFunction
from dune.alugrid import aluConformGrid

# <markdowncell>
# ## The shape functions
# We use a simple class here to collect all required
# information about the finite element space, i.e.,
# how to evaluate the shape functions on the reference
# element (together with their derivatives). We also
# setup a mapper to attach the degrees of freedom to
# the entities of the grid.
# <codecell>
class LinearLagrangeSpace:
    def __init__(self,view):
        self.localDofs = 3
        self.view   = view
        self.dim    = view.dimension
        layout      = lambda gt: 1 if gt.dim == 0 else 0
        self.mapper = view.mapper(layout)
        self.points = numpy.array( [ [0,0],[1,0],[0,1] ] )
    def evaluateLocal(self, x):
        bary = 1.-x[0]-x[1], x[0], x[1]
        return numpy.array( bary )
    def gradientLocal(self, x):
        bary  = 1.-x[0]-x[1], x[0], x[1]
        dbary = [[-1.,-1],[1.,0.],[0.,1.]]
        return numpy.array( dbary )
    def interpolate(self,gf):
        dofs = numpy.zeros(len(space.mapper))
        for e in view.elements:
            dofs[self.mapper(e)] = gf(e,self.points.transpose())
        return dofs

# <markdowncell>
# ### Exercise 1:
# Added quadratic finite element
# <codecell>
class QuadraticLagrangeSpace:
    def __init__(self,view):
        self.localDofs = 6
        self.view   = view
        self.dim    = view.dimension
        layout      = lambda gt: 1 if gt.dim == 0 or gt.dim == 1 else 0
        self.mapper = view.mapper(layout)
        self.points = numpy.array( [ [0,0],[1,0],[0,1],
                                   [0.5,0],[0,0.5],[0.5,0.5] ] )
    def evaluateLocal(self, x):
        bary = 1.-x[0]-x[1], x[0], x[1]
        return numpy.array([ bary[i]*(2.*bary[i]-1.) for i in range(3) ] +\
               [ 4.*bary[(3-j)%3]*bary[(4-j)%3] for j in range(3) ])
    def gradientLocal(self, x):
        bary  = 1.-x[0]-x[1], x[0], x[1]
        dbary = [[-1.,-1],[1.,0.],[0.,1.]]
        return numpy.array([ [ dbary[i][0]*(2.*bary[i]-1.)+bary[i]*2.*dbary[i][0],
                   dbary[i][1]*(2.*bary[i]-1.)+bary[i]*2.*dbary[i][1] ]
                for i in range(3) ] +\
               [ [ 4.*dbary[(3-j)%3][0]*bary[(4-j)%3]+ 4.*bary[(3-j)%3]*dbary[(4-j)%3][0],
                   4.*dbary[(3-j)%3][1]*bary[(4-j)%3]+ 4.*bary[(3-j)%3]*dbary[(4-j)%3][1] ]
                for j in range(3) ])
    def interpolate(self,gf):
        dofs = numpy.zeros(len(space.mapper))
        for e in view.elements:
            dofs[self.mapper(e)] = gf(e,self.points.transpose())
        return dofs

# <markdowncell>
# ## The right hand side and matrix assembly
# We need to iterate over the grid, construct the
# local right hand side and the local system matrix.
# After finishing the quadrature loop we store the
# resulting local matrix in a structure provided by
# the Python package scipy. There are many different
# storage structures available - we use the so called
# 'coordinate' (COO) matrix format which requires us
# construct three vectors, one to store the column
# indices, one for the row indices, and one for the
# values. The convention is that entries appearing
# multiple times are summed up - exactly as we need it.
# So after computing the local matrix and right hand side vector
# $M^E$ we store the values $M^E_{kl}$ into the
# values vector $v_{{\rm start}+3l+k} = M^E_{kl}$
# and the associated global indices
# $c_{{\rm start}+3l+k} = \mu(E,k)$ and
# $r_{{\rm start}+3l+k} = \mu(E,l)$.
# <codecell>
def assemble(space,force,massCoeff,stiffnessCoeff):
    # storage for right hand side
    rhs = numpy.zeros(len(space.mapper))

    # storage for local matrix
    localEntries = space.localDofs
    localMatrix = numpy.zeros([localEntries,localEntries])

    # data structure for global matrix using coordinate (COO) format
    globalEntries = localEntries**2 * space.view.size(0)
    value = numpy.zeros(globalEntries)
    rowIndex, colIndex = numpy.zeros(globalEntries,int), numpy.zeros(globalEntries,int)

    # iterate over grid and assemble right hand side and system matrix
    start = 0
    for e in view.elements:
        geo = e.geometry
        indices = space.mapper(e)
        localMatrix.fill(0)
        for p in quadratureRule(e.type, 4):
            x = p.position
            w = p.weight * geo.integrationElement(x)
            # evaluate the basis function at the quadrature point
            phiVals = space.evaluateLocal(x)
            # assemble the right hand side
            rhs[indices] += w * force(e,x) * phiVals[:]
            # matrix values
            # Exersice:
            jit   = numpy.array(geo.jacobianInverseTransposed(x), copy=False)
            grads = [numpy.dot(jit, dphi) for dphi in space.gradientLocal(x)]
            for i, [phi, dphi] in enumerate( zip(phiVals,grads) ):
                for j, [psi, dpsi] in enumerate( zip(phiVals,grads) ):
                    localMatrix[i,j] += massCoeff*phiVals[i]*phiVals[j] * w
                    localMatrix[i,j] += stiffnessCoeff*numpy.dot(dphi, dpsi) * w
        # store indices and local matrix for COO format
        indices = space.mapper(e)
        for i in range(localEntries):
            for j in range(localEntries):
                entry = start+i*localEntries+j
                value[entry]    = localMatrix[i,j]
                rowIndex[entry] = indices[i]
                colIndex[entry] = indices[j]
        start += localEntries**2

    # convert data structure to compressed row storage (csr)
    matrix = scipy.sparse.coo_matrix((value, (rowIndex, colIndex)),
                         shape=(len(space.mapper),len(space.mapper))).tocsr()
    return rhs,matrix

# <markdowncell>
# ### Exercise 2:
# Computing the L^2 and H^1 error
# <codecell>
def error(u,gradU,uh,gradUh):
    @gridFunction(view)
    def l2Error(e,x):
        return (uh(e,x)-u(e,x))**2
    @gridFunction(view)
    def h1Error(e,x):
        diff = gradUh(e,x)-gradU(e,x)
        return numpy.dot(diff,diff)
    rules = quadratureRules(5)
    l2, h1 = 0,0
    for e in view.elements:
        geometry = e.geometry
        for p in rules(e.type):
            hatx = p.position
            weight = p.weight*geometry.integrationElement(hatx)
            l2 += l2Error(e,hatx)*weight
            h1 += h1Error(e,hatx)*weight
    l2, h1 = math.sqrt(l2), math.sqrt(h1)
    return [l2,h1]

# <markdowncell>
# ## The main part of the code
# Construct the grid and a grid function for the
# right hand side, compute the projection and plot
# on a sequence of global grid refinements:
#
# First construct the grid
# <codecell>
domain = cartesianDomain([0, 0], [1, 1], [10, 10])
view   = aluConformGrid(domain)
# <markdowncell>
# then the grid function to project
# <codecell>
@gridFunction(view)
def u(p):
    x,y = p
    return numpy.cos(2*numpy.pi*x)*numpy.cos(2*numpy.pi*y)
# Exercise 2
@gridFunction(view)
def gradU(p):
    x,y = p
    return numpy.array(
           [-2*numpy.pi*numpy.sin(2*numpy.pi*x)*numpy.cos(2*numpy.pi*y),
            -2*numpy.pi*numpy.cos(2*numpy.pi*x)*numpy.sin(2*numpy.pi*y)])
u.plot(level=3)

massCoeff = 1
stiffnessCoeff = 1
@gridFunction(view)
def forcing(p):
    return u(p)*(stiffnessCoeff*2*(2*numpy.pi)**2+massCoeff)

# <markdowncell>
# and then do the projection on a series of globally refined grids
# <codecell>
oldInterpolationError, oldPDEError = None, None
for ref in range(3):
    space  = QuadraticLagrangeSpace(view)
    print("number of elements:",view.size(0),"number of dofs:",len(space.mapper))
    rhs,matrix = assemble(space, forcing, massCoeff, stiffnessCoeff)
    dofs = space.interpolate(u)
    @gridFunction(view)
    def uh(e,x):
        indices   = space.mapper(e)
        phiVals   = space.evaluateLocal(x)
        localDofs = dofs[indices]
        return numpy.dot(localDofs, phiVals)
    def gradUh(e,x):
        indices   = space.mapper(e)
        jit       = numpy.array(e.geometry.jacobianInverseTransposed(x), copy=False)
        phiGrads  = [numpy.dot(jit, dphi)
                     for dphi in space.gradientLocal(x)]
        # unfortunately: y=jit.mv(x) is not exported - don't know why...
        localDofs = dofs[indices]
        return sum([dof*grad
                    for dof,grad in zip(localDofs,phiGrads)])
    err = error(u,gradU,uh,gradUh)
    if oldInterpolationError is not None:
        eoc = [math.log(oldE/e)/math.log(2) for oldE,e in zip(oldInterpolationError,err)]
    else:
        eoc = None
    print("  interpolation:", err, eoc)
    oldInterpolationError = err

    dofs = scipy.sparse.linalg.spsolve(matrix,rhs)
    err = error(u,gradU,uh,gradUh)
    if oldPDEError is not None:
        eoc = [math.log(oldE/e)/math.log(2) for oldE,e in zip(oldPDEError,err)]
    else:
        eoc = None
    print("  pde problem:", err, eoc)
    oldPDEError = err

    uh.plot(level=1)
    view.hierarchicalGrid.globalRefine(2)
